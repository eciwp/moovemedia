<!DOCTYPE html>
<html class="js csstransitions" lang="en">
<head>
    <meta charset="utf-8">
    <title>Moovemedia</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="description">
    <meta content="wishbone.com" name="author">
    <!--#include file="Includes/_styles.htm" -->
    <!--[if lt IE 9]>
        <script type="text/javascript" src="assets/Scripts/Lib/html5shiv-printshiv.js"></script>
    <![endif]-->
</head>
<body class="policies white__theme">
    <a class="skip-link skip-access" href="#mainContent">Skip to content</a>
     <!-- Header & Menu -->
     <!--#include file="Includes/menu.htm" -->
     <!--// Header & Menu -->
     <a id="mainContent"></a> 
    
    <!-- Body content -->
    <div class="body-wrap content">

        <!-- Page Title -->
        <div class="text-center page-title-wrapper">
            <h1 class="page-title">policies</h1>
        </div><!--// Page Title -->
        
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">

                <!-- Content Policies -->

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  
                  <!-- Accordion Item -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                          <span>Personal Data and Protection Policy</span>
                          <i class="fa fa-angle-up" aria-hidden="true"></i>
                        </a>
                        
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                        
                        <h5>General</h5> 
                        <p>Moove Media Pte Ltd is committed to protecting your personal data and this policy outlines the type of information Moove Media Pte Ltd may collect and how we may use that information.</p>

                        <h5>Personal Data</h5> 
                        <p>Moove Media Pte Ltd respects every individual’s personal identifiable information (e.g. your name, address, telephone number “personal data”) and will not collect any unless it has been provided to us voluntarily. You will not receive any promotional materials from us without providing your personal data.</p>

                        <h5>Use of Information</h5> 
                        <p>When you do provide us with personal data, we may use that information in the following ways, unless stated otherwise: we may store and process that information to better understand your needs and how we can improve our products and services; we (or a fulfillment house or other third party on our behalf in connection with a promotion) may use that information to contact you; and/or we may provide other third parties with aggregate – but not individual – information about visitors or users of our sites. We do not sell, rent or market personal data to third parties. It is Moove Media Pte Ltd’s policy to keep personal data only for as long as needed. At any time, you may request that Moove Media Pte Ltd to correct or remove any such personal data.</p>

                        <h5>Additional Information Collected Automatically</h5> 
                        <p>In some cases, we may automatically (i.e. not via registration) collect technical information when you connect to our site that is not personally identifiable. Examples of this type of information include the type of Internet Browser you are using, the type of computer operating system you are using and the domain name of the website from which you were linked to our site.</p>

                        <p>For any clarifications, please contact us at <a href="mailto:dpo@moovemedia.com.sg" target="_blank">dpo@moovemedia.com.sg</a> or speak to our Data Protection Officer at 6383-7035.</p>

                        <p>Last updated: Apr 2014.</p>
                      </div>
                    </div>
                  </div>
                  <!--// Accordion Item -->

                  <!-- Accordion Item -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                          <span>Disclaimer</span>
                          <i class="fa fa-angle-up" aria-hidden="true"></i>
                        </a>
                      </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                      <div class="panel-body">
                        <p>
                        While Moove Media Pte Ltd (UEN: 200200864C), its member firms, or its related entities ("Moove Media") have taken reasonable care to ensure the accuracy and completeness of the information provided in this document, it will not be liable for any loss or damage of any kind (whether direct, indirect or consequential losses or other economic loss of any kind) suffered due to any omission, error, inaccuracy, incompleteness, or otherwise, any reliance on such information. The information in this document is subject to change without notice.
                        </p>
                      </div>
                    </div>
                  </div>
                  <!--// Accordion Item -->
                  
                  <!-- Accordion Item -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                          <span>QEHS Policy Statement</span>
                          <i class="fa fa-angle-up" aria-hidden="true"></i>
                        </a>
                      </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                      <div class="panel-body">
                        <p>Moove Media Pte Ltd recognises the occupational health and safety of every person in our work premises and conduct business activities in an environmentally responsible manner whilst achieving quality, environmental, health and safety targets.</p>

<h5>Commitment:</h5>
<ol>
    <li>Provide healthy and safe work environment for all our employees, contractors and visitors in order to prevent injury and ill-health.</li>
    <li>Pursue protection of the environment, prevention of pollution and resources conservation.</li>
    <li>Satisfy our customers, applicable regulatory and statutory requirements by delivering products and services first time right.</li>
</ol>

<h5>Compliance:</h5>
We shall take all practicable steps to ensure compliance to applicable quality, environmental, health and safety legal and other requirements.

<h5>Communication:</h5>
We shall strive and establish open communication at various levels so that all employees are aware the importance of meeting customer requirements as well as to prevent of pollution, injury and ill health to all employees, contractors and visitors.

<h5>Continual Improvement:</h5>
<p>
We shall review our quality, environmental, health and safety performance and set achievable and measurable quality, environmental, health and safety objectives and targets for continual improvement.</p>

<p>Updated as of 3 July 2017.</p>

                      </div>
                    </div>
                  </div>
                  <!--// Accordion Item -->

                </div>


                <!--// Content Policies -->

            </div>
          </div>
        </div>

    </div><!--// End Body content -->
    
    <!-- Footer content -->
    <!--#include file="Includes/footer.htm" -->
    <!--// Footer content -->
    <!--#include file="Includes/_scripts.htm" -->
</body>
</html>