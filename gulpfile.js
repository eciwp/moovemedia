var gulp = require('gulp'),
    path = require('path'),
    less = require('gulp-less'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    lessDir = 'assets/less',
    stylesDir = 'assets/Styles',
    scriptsDir = 'assets/Scripts',
    lessDir2 = 'Australia/assets/less',
    stylesDir2 = 'Australia/assets/css',
    scriptsDir2 = 'Australia/assets/js';

/* 
 * build lib.css
 */
gulp.task('lib_styles', function () {
  return gulp.src([stylesDir+'/lib/bootstrap.css',
	stylesDir+'/lib/bootstrap-carousel-fade.css',
    stylesDir+'/lib/galleria.classic.css',
    stylesDir+'/lib/bigvideo.css',
	stylesDir+'/lib/animate.css',
	stylesDir+'/lib/slick.css',
	stylesDir+'/lib/hover.css',
    stylesDir+'/lib/component.css',
    stylesDir+'/lib/platform/landing.css',
    stylesDir+'/lib/platform/inside.css',
    stylesDir+'/lib/creative/grid.css',
    stylesDir+'/lib/samcss.css'])
    .pipe(concat('lib.css'))
    .pipe(gulp.dest(stylesDir))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest(stylesDir));
});

gulp.task('lib_styles_Au', function () {
  return gulp.src([stylesDir2+'/lib/bootstrap.css',
    stylesDir2+'/lib/bootstrap-carousel-fade.css',
    stylesDir2+'/lib/galleria.classic.css',
    stylesDir2+'/lib/bigvideo.css',
    stylesDir2+'/lib/animate.css',
    stylesDir2+'/lib/slick.css',
    stylesDir2+'/lib/hover.css',
    stylesDir2+'/lib/component.css',
    stylesDir2+'/lib/platform/landing.css',
    stylesDir2+'/lib/platform/inside.css',
    stylesDir2+'/lib/creative/grid.css',
    stylesDir2+'/lib/samcss.css'])
    .pipe(concat('lib.css'))
    .pipe(gulp.dest(stylesDir2))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest(stylesDir2));
});

/* 
 * build theme.less to theme.css and theme.min.css
 */
gulp.task('theme_styles', function () {
  return gulp.src(lessDir+'/theme.less')
    .pipe(less())
    .pipe(gulp.dest('.'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest('.'));
});

gulp.task('theme_styles_Au', function () {
  return gulp.src(lessDir2+'/theme.less')
    .pipe(less())
    .pipe(gulp.dest('Australia'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest('Australia'));
});



/* 
 * build lib.js
 */
gulp.task('lib_scripts', function() {
  return gulp.src([
    scriptsDir + '/Lib/jquery-1.9.1.min.js',
	scriptsDir + '/Lib/bootstrap.min.js', 
    scriptsDir + '/Lib/holder.js',
    scriptsDir + '/Lib/jquery.ui.map.full.min.js', 
	scriptsDir + '/Lib/bootstrap-carousel.js', 
	scriptsDir + '/Lib/slick.min.js', 
    scriptsDir + '/Lib/galleria-1.2.9.js',
	scriptsDir + '/Lib/prettyCheckable.js',
    scriptsDir + '/Lib/chosen.jquery.min.js',
	scriptsDir + '/Lib/placeholders.min.js',
	scriptsDir + '/Lib/wow.js',
    scriptsDir + '/Lib/samscript.js',
    scriptsDir + '/Lib/platform/snap.svg-min.js',
    scriptsDir + '/Lib/platform/modernizr.custom.js',
    scriptsDir + '/Lib/platform/grid.js',
    scriptsDir + '/Lib/creative/anime.min.js',
    scriptsDir + '/Lib/creative/main-anime.js',
    scriptsDir + '/Lib/creative/jquery.transform2d.js',
    scriptsDir + '/Lib/creative/jquery.jTinder.js',
    scriptsDir + '/Lib/about/prefixfree.min.js',
    scriptsDir + '/Lib/about/cascade-slider.js',
    scriptsDir + '/Lib/inthenews/modernizr.custom.js',
    scriptsDir + '/Lib/inthenews/masonry.pkgd.min.js',
    scriptsDir + '/Lib/inthenews/imagesloaded.pkgd.min.js',
    scriptsDir + '/Lib/inthenews/classie.js',
    scriptsDir + '/Lib/inthenews/colorfinder-1.1.js',
    scriptsDir + '/Lib/inthenews/gridScrollFx.js'])

    .pipe(concat('lib.js', {newLine: '\n'}))
    .pipe(gulp.dest(scriptsDir))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest(scriptsDir));
});

gulp.task('lib_scripts_Au', function() {
  return gulp.src([
    scriptsDir2 + '/Lib/jquery-1.9.1.min.js',
    scriptsDir2 + '/Lib/bootstrap.min.js', 
    scriptsDir2 + '/Lib/holder.js',
    scriptsDir2 + '/Lib/jquery.ui.map.full.min.js', 
    scriptsDir2 + '/Lib/bootstrap-carousel.js', 
    scriptsDir2 + '/Lib/slick.min.js', 
    scriptsDir2 + '/Lib/galleria-1.2.9.js',
    scriptsDir2 + '/Lib/prettyCheckable.js',
    scriptsDir2 + '/Lib/chosen.jquery.min.js',
    scriptsDir2 + '/Lib/placeholders.min.js',
    scriptsDir2 + '/Lib/wow.js',
    scriptsDir2 + '/Lib/samscript.js',
    scriptsDir2 + '/Lib/platform/snap.svg-min.js',
    scriptsDir2 + '/Lib/platform/modernizr.custom.js',
    scriptsDir2 + '/Lib/platform/grid.js',
    scriptsDir2 + '/Lib/creative/anime.min.js',
    scriptsDir2 + '/Lib/creative/main-anime.js',
    scriptsDir2 + '/Lib/creative/jquery.transform2d.js',
    scriptsDir2 + '/Lib/creative/jquery.jTinder.js',
    scriptsDir2 + '/Lib/about/prefixfree.min.js',
    scriptsDir2 + '/Lib/about/cascade-slider.js',
    scriptsDir2 + '/Lib/inthenews/modernizr.custom.js',
    scriptsDir2 + '/Lib/inthenews/masonry.pkgd.min.js',
    scriptsDir2 + '/Lib/inthenews/imagesloaded.pkgd.min.js',
    scriptsDir2 + '/Lib/inthenews/classie.js',
    scriptsDir2 + '/Lib/inthenews/colorfinder-1.1.js',
    scriptsDir2 + '/Lib/inthenews/gridScrollFx.js'])

    .pipe(concat('lib.js', {newLine: '\n'}))
    .pipe(gulp.dest(scriptsDir2))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest(scriptsDir2));
});


/* 
 * lint content.js and minify it to content.min.js
 * more info on js linting using jshint: http://jshint.com/docs/
 */
gulp.task('content_scripts', function() {
  return gulp.src(scriptsDir+'/content.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest(scriptsDir))
});

gulp.task('content_scripts_Au', function() {
  return gulp.src(scriptsDir2+'/content.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest(scriptsDir2))
});


gulp.task('watch', function() {
  gulp.watch(lessDir+'/*.less', ['lib_styles']);
  gulp.watch('assets/less/*.less', ['theme_styles']);
 
  gulp.watch(scriptsDir + '/Lib/*.js', ['lib_scripts']);
  gulp.watch(scriptsDir+'/content.js', ['content_scripts']);

  gulp.watch(lessDir2+'/*.less', ['lib_styles_Au']);
  gulp.watch('Australia/assets/less/*.less', ['theme_styles_Au']);
 
  gulp.watch(scriptsDir2 + '/Lib/*.js', ['lib_scripts_Au']);
  gulp.watch(scriptsDir2 +'/content.js', ['content_scripts_Au']);
  
});

gulp.task('default', ['lib_styles', 'theme_styles', 'lib_scripts','content_scripts', 'lib_styles_Au', 'theme_styles_Au', 'lib_scripts_Au','content_scripts_Au', 'watch']);