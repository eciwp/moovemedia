$(document).ready(function() {
    "use strict";
    init_platform_landing();
    initHomeImages();
    initHeaderMenu();
    initPopUpPromotion();
    initRateList();
});

function init_platform_landing() {
    "use strict";
    var speed = 330,
        easing = mina.backout;
    [].slice.call(document.querySelectorAll('#platform-grid > a')).forEach(function(el) {
        var s = Snap(el.querySelector('svg')),
            path = s.select('path'),
            pathConfig = {
                from: path.attr('d'),
                to: el.getAttribute('data-path-hover')
            };
        el.addEventListener('mouseenter', function() {
            path.animate({
                'path': pathConfig.to
            }, speed, easing);
        });
        el.addEventListener('mouseleave', function() {
            path.animate({
                'path': pathConfig.from
            }, speed, easing);
        });
    });
}

/* HOME IMAGES */
function initHomeImages() {
    "use strict";
    var wow = new WOW({
        boxClass: 'wow',
        animateClass: 'animated',
        offset: 0,
        mobile: true
    });
    wow.init();
    $('.home-hero .btn.btn-down-arrow').bind('click', function() {
        $('html, body').animate({
            scrollTop: $('section.home-images').offset().top - 80
        }, 1000);
    });
}

/* HEADER MENU */
function initHeaderMenu() {
    "use strict";
    $('.menu-mobile-btn').bind('click', function() {
        $(this).find('a').toggleClass('nav-is-visible');
        $('.main-navigation .main-navigation-wrapper-nav').slideToggle();
        if ($(window).width() <= 992) {
            if ($('.search-bar-wrapper').is(':visible')) {
                $('.search-bar-wrapper').hide();
            }
        }
    });
    $('.search-button').bind('click', function() {
        $('.search-bar-wrapper').slideToggle();

        if ($(window).width() <= 992) {
            if ($('.main-navigation .main-navigation-wrapper-nav').is(':visible')) {
                $('.menu-mobile-btn a').removeClass('nav-is-visible');
                $('.main-navigation .main-navigation-wrapper-nav').hide();
            }
        }
        if ($('.main-navigation .main-navigation-wrapper-nav').is(':visible')) {
            $('#searchTextbox').trigger('focus');
        }
    });

    if ($('#searchTextbox').val() === '') {
        $('#searchTextbox').closest('form').removeClass('focus');
    }else{
        $('#searchTextbox').closest('form').addClass('focus');
    }

    $('#searchTextbox').bind('focus', function() {
        $(this).closest('form').addClass('focus');
    });
    $('#searchTextbox').bind('focusout', function() {
        if ($(this).val() === '') {
            $(this).closest('form').removeClass('focus');
        }
    });
    $(window).resize(function() {
        if ($(window).width() > 992) {
            $('.menu-mobile-btn a').removeClass('nav-is-visible');
            $('.main-navigation .main-navigation-wrapper-nav').removeAttr('style');
        }
    });
}

/* PROMOTIONS */
function initPopUpPromotion() {
    "use strict";
    $(".pop-up-promotion-content .img img").on("load", function() {
      $('.pop-up-promotion-wrapper').addClass('visible');
    }).each(function() {
      if(this.complete){
        //$(this).load();
        $('.pop-up-promotion-wrapper').addClass('visible');
      }
    });

    $('.close-popup, .pop-up-promotion-overlay').bind('click', function() {
        if ($(window).width() >800) {
            $('.pop-up-promotion-wrapper').addClass('hide-promo');
            $('.link-findoutmore').text('promotions');
        }else{
            $('.pop-up-promotion-wrapper').removeClass('visible');
            $('#promotion-mobile').addClass('bounceInUp');
        }
    });
    $('.pop-up-promotion-content .img > div').bind('click', function() {
        $('.pop-up-promotion-wrapper').removeClass('hide-promo');
        $('.link-findoutmore').text('find out more');
    });

    $('#promotion-mobile').bind('click', function() {
        $('#promotion-mobile').removeClass('bounceInUp');
        $('.pop-up-promotion-wrapper').addClass('visible');
    });
}

/* RATE */
function initRateList() {
    "use strict";
    var listItem = $('.rate-list li'),
        arHeight = [],
        height1, height2, theIndex;
    listItem.each(function(index, element) {
        theIndex = index + 1;
        if (theIndex % 2 === 0) {
            arHeight = [];
            height1 = $(element).height();
            height2 = $(element).prev().height();
            arHeight.push(height1);
            arHeight.push(height2);
            $(element).height(Math.max.apply(Math, arHeight));
            $(element).prev().height(Math.max.apply(Math, arHeight));
        }
    });
}