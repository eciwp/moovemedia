var $J = jQuery.noConflict();

/* function to check if the selector exists or if the selector returns results */
$J.fn.exists = function(){return this.length > 0;}

$J(document).ready(function () {

	initAccordionIndicator();

	initAccordionCollapseNext();

	initMainNavHover();
	//initMasonryLayout();

	initStickyFooter();
	
	initGmap();
	
	initShortenText({
		wrapperClassName	: '.search-results',	
		contentClassName	: '.wysiwyg-content',
		linkClassName		: '.btn-mini',
		showChars			: 200
	});
	
	initGallery();
	
	initShortenText({
		wrapperClassName	: '.article-list',	
		contentClassName	: '.wysiwyg-content',
		linkClassName		: '.btn-mini',
		showChars			: 360,
		ellipsesText		: '......&nbsp;&nbsp;'
	});

	initPrettyCheckable();

	initChosen();

	// initCarousel();

	initSlickCarousel();
	initSlickGallery();
	
	
	initSearchForm();

	initPromoCarousel();

	initFooterMenu();
	
	//initBodyClicked();

	initChangeTheNoFileChosen();
	initStopVideoPlay();
	initCheckContenSpan();
	initKeysEnterSearch();
	initVideoLightbox();
	initFileFieldFocus();
})

$J(window).load(function () {
  initCarouselEqualizeHeight();
});

$J(window).resize(function() {
   initCarouselEqualizeHeight();
});

$J(window).on('resize', function () { 	
	// repositioning gmap's marker everytime window resized
	$J('.google-map').each(function(){
		var map 		= $J(this).gmap("get", "map");
		var	currCenter 	= map.getCenter();		
		google.maps.event.trigger(map, 'resize');
		map.setCenter(currCenter);
	});
});

/* Init search form header clicked */
function initSearchForm(){
	$J('.btn-main-search').click(function(e){
		e.preventDefault();
		$J(this).addClass('hide');
		$J('.search-query-wrap').addClass('active');
		//$J('.search-query').focus();
	});

	/* close search form on body clicked */
	$J('body').bind('touchstart click',function(e){
		if(! $J(e.target).is('.icon-search, .search-query') ){
		setTimeout(function(){
			$J('.btn-main-search').removeClass('hide');
		},300);
			$J('.search-query-wrap').removeClass('active');		
		}
	});

	/* show search form on focus */
	$J('body').on('focus', '.search-query', function(){
		if(!$J('.search-query-wrap').hasClass('active'))
			$J('.btn-main-search').trigger('click');
	});
}

/* Init Accordion Indicator - Adds and removes classes for the accordion indicator styles to change when it opens and closes */
function initAccordionIndicator(){
	$J('.accordion').each(function(){
		var accordionBody = $J(this).find('.accordion-body');
		accordionBody.on('hide',function(){
			$J(this).prev().removeClass('in')
		}); 
		accordionBody.on('show',function(){
			$J(this).prev().addClass('in')
		});
		accordionBody.each(function(){
			if($J(this).hasClass('in'))
				$J(this).prev().addClass('in')})
	});
}

/* Init Accordion Collapse Next - Initialize Accordion [data-toggle="collapse"] replaced with [data-toggle"collapse-next"] used by accordion which without ID's for each accordion's header and body content */
function initAccordionCollapseNext(){
	$J('.accordion-body.collapse').collapse({toggle: false});
	$J('.accordion-toggle[data-toggle="collapse-next"]').on('click',function(e){ 
		e.preventDefault()
		$J(this).parent().next('.accordion-body.collapse').collapse('toggle')
	})	
}

/* Init main navigation on hover 
* adds and removes classes hover on desktop view 
* show child and enable tab nagivation on focus
*/
function initMainNavHover(){
	var view = parseFloat($J(window).width());
	if(view <= 768){
		return;
	}
	$J('.main-navigation ul.nav > li').hover(
		function(){$J(this).addClass('hover')}, 		
		function(){
			$J(this).removeClass('hover');
			if(view>767){
				$J(this).parent().find('.open').removeClass('open')
			}
	});
	
	
	if(view>767){
		$J('body').on('focus', '.dropdown a', function(){
			var el = $J(this);
			$J('.dropdown').removeClass('open hover');
			el.parents('.dropdown').addClass('open hover');
		})
		.on('blur', '.dropdown a', function(){
			var el = $J(this);
			if(el.parent().is(':last-child') && !el.hasClass('dropdown-toggle')) {
				var dropdown = el.parents('.dropdown');
				if(typeof(dropdown) != 'undefined') {
					if(dropdown.length == 1){
						dropdown.removeClass('open hover');
					}else if(dropdown.length > 1){
						dropdown.first().removeClass('open hover');
					}
				}
			}

		});
	}
}

/* Init Masonry Layout - adds and removes class ".masonry-layout" to the wrapper/parent of column items
function initMasonryLayout(){
	$J('.masonry-layout').each(function(){	
		var context 		= $J(this),
			children 		= context.children(),
			containerWidth  = context.width(),
			cls 			= children.attr("class").split(" ");
			
		for (var i in cls) 
			if (/col-md-/.test(cls[i])) {
				var noColumnClsName = cls[i];
				break;
			}
		
		var noColumnClsSplit	= noColumnClsName.split("col-md-"),
			noColumn			= 12 / parseFloat(noColumnClsSplit[1]);		
		
		context.masonry({
			itemSelector	: children,
			columnWidth		: function( containerWidth ) {
								return containerWidth / noColumn;	
							}
		})
	})
	} */

/* init sticky footer using js for dinamic footer height */
function initStickyFooter() {
	// make sure all image, font style and style done before call it for the first time
	$J(window).load(function () {
		setMinHeight();
	});

	// recall on every window resize
	$J(window).bind('resize', function () { 
		setMinHeight();
	});

	function setMinHeight() {
		// reset height for recalculation
		$J('.footer').prev().css({ 'min-height': '' });

		// set min heigth for tag before footer if needed
		var bodyHeight = $J(document.body).height(), windowHeight = $J(window).height(), beforeFooter = $J('.footer').prev();

		if(bodyHeight < windowHeight) {
			beforeFooter.css({ 'min-height': ((windowHeight - bodyHeight) + beforeFooter.height()) + 'px' });
		}
	}
}

/* Init jquery Google Map. Reference: https://code.google.com/p/jquery-ui-map/ */
function initGmap(){
	$J('.google-map').each(function(){
		var dataSrcObj = window[$J(this).attr('data-src')];		
		$J(this).gmap({
				'center'			: dataSrcObj.startingLoc, 
				'zoom'				: dataSrcObj.startZoom, 
				'callback'			: function() {					
					for(i=0; i < dataSrcObj.markerArray.length; i++){
						var self = this;
						self
							.addMarker({
								'id'		: 'gmapID-'+i,
								'position'	: dataSrcObj.markerArray[i].markerLoc,  
								'content'	: dataSrcObj.markerArray[i].markerContent})
							.click(function() {
								self.openInfoWindow({ 
									'content': this.content
								}, this); 
							})
					}
				}
		})		
	})	
}

/* Init shorten text, with 3 variables are important to be declared when use this function ".wrapperClassName, .contentClassName, .linkClassName " */
function initShortenText(option){
	var config = {
		wrapperClassName 	: '',	
		contentClassName	: '',
		linkClassName		: '',	
		showChars			: 150, 
		ellipsesText 		: "&nbsp;...&nbsp;"
	}
	
	var encodeHTML = function(string) {
		var element = $J('<div></div>');
		element.html(string);
		return element.html();
	}
	
	if (option)	$J.extend(config, option);	
	if (config.wrapperClassName !='' && config.contentClassName !='' &&	config.linkClassName !='' && $J(config.wrapperClassName).exists() && $J(config.contentClassName).exists() && $J(config.linkClassName).exists() ){		
	$J(config.wrapperClassName)
		.find(config.contentClassName)
		.each(function(){				
			var self 		= $J(this),
				content 	= self.html(),
				readMoreBtn	= self.find(config.linkClassName).get(0).outerHTML;
			
			if (content.length > config.showChars) {
				var newHTML,
					temp		= $J('<div></div>'),
					section		= content.substr(0, config.showChars),
					moreLink	= config.ellipsesText + readMoreBtn;
										
				temp.html(encodeHTML(section));	
				newHTML = ( temp.find('p').length > 0 ) ? temp.find('p:last').append(moreLink) : temp.append(moreLink);
				self.html(newHTML);
			}
		});
	}
}

function initGallery() {
	if ($J('.galleria').exists()) {
		Galleria.loadTheme('Assets/Scripts/Lib/galleria.classic.js');
		Galleria.run('.galleria');
	}
}

function initPrettyCheckable() {
	$J('input[type=radio], input[type=checkbox]').prettyCheckable({
		color: 'blue'
	});
}

function initChosen() {
	$J('select').chosen();
    $J('.chzn-search input').each(function(){
        var contWidth = $J(this).parents('.chzn-container').outerWidth();
        var contHeight = $J(this).parents('.chzn-container').outerHeight();
        $J(this).focus(function(){
            $J(this).parents('.chzn-drop').css({'left':'0', 'width':contWidth, 'top':contHeight});
            $J(this).parents('.chzn-drop').find('li.active-result').each(function(){
                $J(this).click(function(){
                    $J(this).parents('.chzn-drop').css({'left':'-9000px', 'width':contWidth, 'top':contHeight});
                    //console.log('kenaklik');
                });
            });
        });

    });
}

function initCarousel() {
	if(!$J('.carousel.slide'))
		return;

	$J('.carousel.slide').each(function(){
		var el = $J(this)
		el.carousel({
			interval: el.data('interval') ? el.data('interval') : 5000,
			pause: 'hover'
		})
	});

}

function initSlickCarousel() {
	if(!$J('.slick-carousel').length)
		return;

	$J('.slick-carousel').each(function(){
		$J(this).slick({
  			dots: true,
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: $J(this).data('autoplay') ? true : false,
			autoplaySpeed: $J(this).data('interval'),
			speed:  $J(this).data('speed'),
			prevArrow: '<a href="#" class="carousel-control left">&lsaquo;<span class="access">Transition to the previous slide</span></a>',
			nextArrow: '<a href="#" class="carousel-control right">&rsaquo;<span class="access">Transition to the next slide</span></a>',
			customPaging: function(slider, i) {
                return '<button type="button" data-role="none">*Transition to slide *' + (i + 1) + '</button>';
            },
			onAfterChange: function() {
			    // $J('.item').mouseenter();
			}
		});
	});

}
function initSlickGallery() {
	if(!$J('.slick-gallery').length)
		return;
	//add numbering use on doc ready calculation
	var totalslider = $J('.slick-gallery .item').length;
	$J('.slick-gallery .item  .numbering').each(function(i){					
			$J(this).html(i + 1 + '/' + totalslider);			
		});
		
	$J('.slick-gallery').each(function(){
		$J(this).slick({
  			dots: false,
			slidesToShow: 1,
			slidesToScroll: 1,
  			adaptiveHeight: true,
			autoplay: $J(this).data('autoplay') ? true : false,
			autoplaySpeed: $J(this).data('interval'),
			speed:  $J(this).data('speed'),		
			prevArrow: '<a href="#" class="carousel-control left">&lsaquo;<span class="access">Transition to the previous slide</span></a>',
			nextArrow: '<a href="#" class="carousel-control right">&rsaquo;<span class="access">Transition to the next slide</span></a>',
			onAfterChange: function() {
			    // $J('.item').mouseenter();
			}/*,
			customPaging: function (slider, i) {
				console.log(slider);
				return  (i + 1) + '/' + slider.slideCount;				
			}*/
		});
		
	});

}

function initPromoCarousel() {
	var elmPromoCarousel = $J('.promo-carousel');
	if(!elmPromoCarousel.length)
		return;

	elmPromoCarousel.carousel({
		pause: 'hover'
	});

	var elmIndicator = elmPromoCarousel.find('.carousel-indicators li');

	elmIndicator.each(function(index, el) {
		$J(this).on('click', function(event) {
			event.preventDefault();
			var numIndicator = $J(this).data('slide-to');
			var elmPromoCarouselThis = $J(this).parents('.promo-carousel');
			elmPromoCarouselThis.find('.carousel-indicators').children('li').removeClass('active');
			elmPromoCarouselThis.carousel(numIndicator);
			$J(this).addClass('active');
		});
		
	});

	elmPromoCarousel.bind('slid', function() {
	    currentIndex = $J('div.active').index();
	    elmIndicator.eq(currentIndex).addClass('active');
	});

	//carousel control navigation
	var elmCarouselNav = $J('.carousel-control-nav');
	if(!elmCarouselNav.length)
		return;
	var elmPause = elmCarouselNav.children('.pause'),//pause button
		elmPlay = elmCarouselNav.children('.play');//play button

	elmPause.bind('touchstart click', function(event) {
		var elmPromoCarouselThis = $J(this).parents('.promo-carousel');
		elmPromoCarouselThis.carousel('pause');
		elmPromoCarouselThis.find('.play').removeClass('hide');
		$J(this).addClass('hide');
		return false;
	});

	elmPlay.bind('touchstart click', function(event) {
		var elmPromoCarouselThis = $J(this).parents('.promo-carousel');
		elmPromoCarouselThis.carousel('cycle');
		elmPromoCarouselThis.find('.pause').removeClass('hide');
		$J(this).addClass('hide');
		return false;
	});

	initPromoCarouselEqualizeHeight();
}

function initPromoCarouselEqualizeHeight() {		
    function equalizeHeight() {		
        $J(".promo-carousel .item").css('height', "auto");		
        $J(".promo-carousel").each(function () {		
            var height = 0;		
            $J(this)		
                .find('.item').each(function () {		
                    height = Math.max(height, $J(this).height());		
                })		
                .css('height', height);		
        });		
    }		
    $J(window).on('resize orientationchange', function () {		
        equalizeHeight(); 		
    });		
    $J(window).load(function () {
	  equalizeHeight();
	});
}

function initFooterMenu() {
	
	var container = $J(".footer-top-wrap .footer-section-wrap > .nav");
	if (!container.length)
		return;

	$J(window).resize(function(event) {
		generateFooter($J(this).outerWidth());
	});

	generateFooter($J(window).outerWidth());

	function generateFooter(windowSize){
		if(windowSize<=768){
			
			if(!container.hasClass('footerNavigation')){
				container.addClass('footerNavigation');
				//container.find("li.section").find("ul").collapse("hide");
				
				container.find("li.section").find("ul").addClass("collapse");

				container.find('li.section > a').bind('click', function (e) {
					e.preventDefault();

					console.log("test");

					var that = $J(this),
						parent = that.closest('li.section');
					if(parent.hasClass('active in')) {
						parent.removeClass('active in').children("ul").collapse("toggle");
					} else {
						parent.closest("ul").find('.active.in').removeClass('active in').children("ul").collapse("toggle");
						parent.addClass('active in').children("ul").collapse("toggle");
					}
				});
			}
			
			
			
		}
		else {
			if(container.hasClass('footerNavigation')){
				container.removeClass('footerNavigation');
				container.find('li.section').removeClass('active in').find('ul').removeClass('collapse').removeClass('in').removeAttr('style');
				container.find('li.section > a').unbind('click');
			}
		}
	}
	
}

function initChangeTheNoFileChosen(){
	window.changeLabelFile = function(){
	    var fileId = document.getElementById('fileInput');
	    var msgChosen = "No file selected";
		if(fileId.value == "")
		    {
		        fileLabel.innerHTML = msgChosen;
		    }
		else
		    {
		        var theSplit = fileId.value.split('\\');
		        fileLabel.innerHTML = theSplit[theSplit.length-1];
		    }
		}
}

function initStopVideoPlay(){
	$J('.close-videocorousel-home').click(function(){
		  var vid = $J('.modal-body iframe[src*="youtube"]');
		  if ( vid.length > 0 ){
	    var src = vid.attr('src');
	    // (this will kill the video playing)
	    vid.attr('src', '');
	    vid.attr('src', src);
	  }
	});	
}

function initCheckContenSpan(){
	var contenName = {
		rightSpan3 :'.content-right.col-md-3',
		leftSpan9 :'.content-left.col-md-9',
		rightSpan4 :'.content-right.col-md-4',
		leftSpan8 :'.content-left.col-md-8',
		//this var make full content
		leftSpan12Attr :'content-left col-md-12'
	}	
	if($J(contenName.rightSpan3).is(':empty')){
		//this for right span 3
		$J(contenName.rightSpan3).remove();
		$J(contenName.leftSpan9).attr('class',contenName.leftSpan12Attr);
	}
	if($J(contenName.rightSpan4).is(':empty')){
		//this for right span 4
		$J(contenName.rightSpan4).remove();
		$J(contenName.leftSpan4).attr('class',contenName.leftSpan12Attr);
	}
}


function initKeysEnterSearch() {
    var keyAttr = {
        keyAttrSearch: 'search-result?term='
    };
	var openNewTab ='/'+keyAttr.keyAttrSearch;
	var varKey =$J('#searchTextbox').val();
	$J('.navbar-search .icon-search').click(function(){
		var parentEl = $J(this).parents('.search-query-wrap');
		var parentElMobile = $J(this).parents('.mobile-search');
		if(parentEl.length){
			varKey = parentEl.find('.search-query').val();
		}else if(parentElMobile.length){
			varKey = parentElMobile.find('.search-query').val();
		}
		if(varKey==''){}else{
			if (!checkScriptValid(varKey)) {
			   alert(varKey+' is wrong value.');
			}else{
				window.open(openNewTab+''+varKey, '_self');
			}
		}
	});
	$J('.navbar-search .search-query').keypress(function(event){
		varKey =$J(this).val();
		if (event.which == 13) {
        	event.preventDefault();
        	if (!checkScriptValid(varKey)) {
			   alert(varKey+' is wrong value.');
			}else{
				window.open(openNewTab+''+$J(this).val(), '_self');
			}
			
   		 }
	});		
}

function checkScriptValid(param){
	var seacrhRegex = /^[A-Za-z][-a-zA-Z ]+$/; //note that the hyphen has been moved
	if (!seacrhRegex.test(param)) {
		return false;
	}else{
		return true;
	}
	return false;
}

function initVideoLightbox() {
    var vidId, vidTitle, vidCaption, vidTranscript, vidAudio,
    	elmLightboxModal = $J('.lightbox-old');

    $J('.control-play-video').on('click', function () {
        vidId = $J(this).data('video-id');
        vidTitle = $J(this).data('video-title');
        vidCaption = $J(this).data('video-caption');
        vidTranscript = $J(this).data('transcript');
        vidAudio = $J(this).data('video-audio-id');

        $J('p.transcript-text').hide();
        $J('p.alternate-vid').hide();

        if (vidTranscript != undefined && vidTranscript.length>0) {
        	 $J('p.transcript-text').show();
        	 $J('p.transcript-text a').attr('href',vidTranscript)
        }

        if(vidAudio != undefined && vidAudio.length>0)
        	$J('p.alternate-vid').show();
        
    });
    
    elmLightboxModal.on('show.bs.modal', function () {
    	var elmLightboxModalThis = $J(this);
		elmLightboxModalThis.find('.modal-body').html('<iframe title="' + vidTitle + '" src="//www.youtube.com/embed/' + vidId + '?rel=0&amp;autohide=1&amp;hd=1&amp;modestbranding=1&amp;wmode=opaque&amp;showinfo=0&amp;autoplay=1" frameborder="0" allowfullscreen></iframe>');
        elmLightboxModalThis.find('.modal-body').append('<p>' + vidCaption + '</p>');
        elmLightboxModalThis.find('.modal-header > h1').text(vidTitle);
        
        //focus on close button
        setTimeout(function () {
            elmLightboxModalThis.find('.close').focus();
        }, 1000);

    });

    elmLightboxModal.on('hide.bs.modal', function () {
    	var elmLightboxModalThis = $J(this);
        elmLightboxModalThis.find('.modal-body').html('');
    });

    $J('p.alternate-vid > a').on('click', function (event) {
        event.preventDefault();
        var elmIframe = $J(this).parents('.modal').find('.modal-body > iframe'),
            srcVal = "//www.youtube.com/embed/" + vidAudio + "?rel=0&amp;autohide=1&amp;hd=1&amp;modestbranding=1&amp;wmode=opaque&amp;showinfo=0&amp;autoplay=1";

        elmIframe.attr('src', srcVal);
    });
}

function initFileFieldFocus() {
	var elmFile = $J('.btn-file-forfile input[type="file"]');
	if(!elmFile.length)
		return;

	elmFile.focus(function(event) {
		elmFile.parent('.btn-file-forfile').addClass('focus');
	});

	elmFile.blur(function(event) {
		elmFile.parent('.btn-file-forfile').removeClass('focus');
	});
}		

function initCarouselEqualizeHeight() {
  $J('.carousel .item, .carousel .carousel-caption').css('min-height', '');
  $J('.carousel').each(function() {
  	if($J(this).hasClass('promo-carousel') || $J(this).find('.slick-gallery').length)
  		return;
  	
    var carouselItem = $J(this).find('.item'),
        carouselItemMaxHeight = 0;
    carouselItem.each(function() {
      if ( $J(this).height() > carouselItemMaxHeight ) {
        carouselItemMaxHeight = $J(this).height();
      }
    });

    if( $J(this).parents('.col-md-4').length ) {
      	carouselItemMaxHeight = carouselItemMaxHeight + 30;
    }

    if ( $J(window).width() > 991 ) {
		carouselItem.css('min-height', carouselItemMaxHeight);
	}else{
		carouselItem.css('min-height', '');
	}
    // if ( $J(window).width() > 991 ) {
    //   carouselItem.find('.carousel-caption').css('min-height', carouselItemMaxHeight);
    // } else {
    //   carouselItem.find('.carousel-caption').css('min-height', '');
    // }
  });
}