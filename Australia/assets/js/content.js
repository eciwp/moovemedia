$(document).ready(function() {
    "use strict";
    init_platform_landing();
    initHomeImages();
    initHeaderMenu();
    initPricingList();
    initWow();
    initFeatureThumbnail();

});
 $(window).resize(function() {
    "use strict";
    initFeatureThumbnail();
    initPricingList();
    
});

function init_platform_landing() {
    "use strict";
    var speed = 330,
        easing = mina.backout;
    [].slice.call(document.querySelectorAll('#platform-grid > a')).forEach(function(el) {
        var s = Snap(el.querySelector('svg')),
            path = s.select('path'),
            pathConfig = {
                from: path.attr('d'),
                to: el.getAttribute('data-path-hover')
            };
        el.addEventListener('mouseenter', function() {
            path.animate({
                'path': pathConfig.to
            }, speed, easing);
        });
        el.addEventListener('mouseleave', function() {
            path.animate({
                'path': pathConfig.from
            }, speed, easing);
        });
    });
}

/* HOME Slider */
function initHomeImages() {
    "use strict";
    if(!$('.slick-carousel').length){
        return;
    }

    $('.slick-carousel').slick({
      rtl: true
    });
   
}

/* Init WOW */
function initWow() {
    "use strict";
    var wow = new WOW({
        boxClass: 'wow',
        animateClass: 'animated',
        offset: 0,
        mobile: true
    });
    wow.init();
   
}

/* HEADER MENU */
function initHeaderMenu() {
    "use strict";
    $('.menu-mobile-btn').bind('click', function() {
        $(this).find('a').toggleClass('nav-is-visible');
        $('.main-navigation .main-navigation-wrapper-nav').slideToggle();
        if ($(window).width() <= 1010) {
            if ($('.search-bar-wrapper').is(':visible')) {
                $('.search-bar-wrapper').hide();
            }
        }
    });
    $('.search-button').bind('click', function() {
        $('.search-bar-wrapper').slideToggle();
        if ($(window).width() <= 1010) {
            if ($('.main-navigation .main-navigation-wrapper-nav').is(':visible')) {
                $('.menu-mobile-btn a').removeClass('nav-is-visible');
                $('.main-navigation .main-navigation-wrapper-nav').hide();
            }
        }
        if ($('.main-navigation .main-navigation-wrapper-nav').is(':visible')) {
            $('#searchTextbox').trigger('focus');
        }
    });

    if ($('#searchTextbox').val() === '') {
        $('#searchTextbox').closest('form').removeClass('focus');
    }else{
        $('#searchTextbox').closest('form').addClass('focus');
    }

    $('#searchTextbox').bind('focus', function() {
        $(this).closest('form').addClass('focus');
    });
    $('#searchTextbox').bind('focusout', function() {
        if ($(this).val() === '') {
            $(this).closest('form').removeClass('focus');
        }
    });


    $(window).resize(function() {
        if ($(window).width() > 1010) {
            $('.menu-mobile-btn a').removeClass('nav-is-visible');
            $('.main-navigation .main-navigation-wrapper-nav').removeAttr('style');
        }
    });
}

/* FEATURE THUMBNAIL */
function initFeatureThumbnail() {
    "use strict";

    if(!$('.feature-thumbnail-box').length){
        return;
    }

    var listThumbnail = $('.feature-thumbnail-box'),
        arHeightRow1 = [], arHeightRow2 = [],
        heightRow1, heightRow2;

    if ($(window).width() >= 768) {
        listThumbnail.each(function(index, element) {
                arHeightRow1 = [];
                arHeightRow2 = [];

                $(element).find('li').each(function(){
                    heightRow1 = $(this).find('.feature-row1').height();
                    heightRow2 = $(this).find('.feature-text').height();
                    arHeightRow1.push(heightRow1);
                    arHeightRow2.push(heightRow2);
                
                });

                $(element).find('.feature-row1').height(Math.max.apply(Math, arHeightRow1));
                $(element).find('.feature-text').height(Math.max.apply(Math, arHeightRow2));
        });
    }else{
        $('.feature-thumbnail-box .feature-row1, .feature-thumbnail-box .feature-text').removeAttr('style');
    }
}

/* PRICING */
function initPricingList() {
    "use strict";

    if(!$('.pricing-list').length){
        return;
    }

    var listItem = $('.pricing-list li'),
        arHeightTitle = [], arHeightDesc = [],
        heightTitle1, heightTitle2, heightDesc1, heightDesc2, theIndex;
    listItem.each(function(index, element) {
        theIndex = index + 1;
        if (theIndex % 2 === 0) {
            arHeightTitle = [];
            heightTitle1 = $(element).find('.pricing-item-title').height();
            heightTitle2 = $(element).prev().find('.pricing-item-title').height();
            arHeightTitle.push(heightTitle1);
            arHeightTitle.push(heightTitle2);
            $(element).find('.pricing-item-title').height(Math.max.apply(Math, arHeightTitle));
            $(element).prev().find('.pricing-item-title').height(Math.max.apply(Math, arHeightTitle));

            arHeightDesc = [];
            heightDesc1 = $(element).find('.pricing-item-desc-content').height();
            heightDesc2 = $(element).prev().find('.pricing-item-desc-content').height();

            arHeightDesc.push(heightDesc1);
            arHeightDesc.push(heightDesc2);


            $(element).find('.pricing-item-desc').height(Math.max.apply(Math, arHeightDesc));
            $(element).prev().find('.pricing-item-desc').height(Math.max.apply(Math, arHeightDesc));

        }
    });
}