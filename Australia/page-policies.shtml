<!DOCTYPE html>
<html class="js csstransitions" lang="en">
<head>
    <meta charset="utf-8">
    <title>Moovemedia</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="description">
    <meta content="wishbone.com" name="author">
    <!--#include file="Includes/_styles.htm" -->
    <!--[if lt IE 9]>
        <script type="text/javascript" src="assets/Scripts/Lib/html5shiv-printshiv.js"></script>
    <![endif]-->
</head>
<body class="policies">
    <a class="skip-link skip-access" href="#mainContent">Skip to content</a>
     <!-- Header & Menu -->
     <!--#include file="Includes/menu.htm" -->
     <!--// Header & Menu -->
     <a id="mainContent"></a> 
    
    <!-- Body content -->
    <div class="body-wrap content">

        <!-- Page Title -->
        <div class="text-center page-title-wrapper">
            <h1 class="page-title">Privacy Policy</h1>
        </div><!--// Page Title -->
        
        <section class="container-fluid">

          <div class="row">
            <div class="col-md-12">

              <!-- POLICIES INTRO TEXT -->
              <div class="intro-text">
                  <p><strong>Moove Media Australia Pty Ltd</strong> (ABN 97 147 649 938) (<strong>'Moove Media Australia', 'we''our'</strong> and <strong>'us'</strong>) and its related entities are committed to responsible privacy practices and to complying with the Australian Privacy Principles contained in the Privacy Act 1988 (Cth) (<strong>'Privacy Act'</strong>).</p>
<p>This privacy policy describes how we manage personal information including how we collect personal information, the purposes for which we use this information and to whom this information is disclosed. We may change our Privacy Policy from time to time at our discretion.</p>
              </div>
              <!--// POLICIES INTRO TEXT -->

            </div>
          </div>

          <div class="row">
            <div class="col-md-12">

                <!-- Content Policies -->

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  
                  <!-- Accordion Item -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading1">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                          <span>Personal Information</span>
                          <i class="fa fa-angle-up" aria-hidden="true"></i>
                        </a>
                        
                      </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                      <div class="panel-body">
                        
                        <p>In this Privacy Policy, 'personal information' has the meaning set out in the Privacy Act, being information or an opinion about an identified individual or an individual who is reasonably identifiable.</p>
                      </div>
                    </div>
                  </div>
                  <!--// Accordion Item -->

                  <!-- Accordion Item -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading2">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                          <span>Personal Information We May Collect</span>
                          <i class="fa fa-angle-up" aria-hidden="true"></i>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                      <div class="panel-body">
                        
                          <strong>The types of personal information Moove Media Australia collects from you depends on the circumstances in which the information is collected, which may include:</strong>
                          <ul>
                            <li>An individual contacting us with an enquiry</li>
                            <li>Contact in relation to booking of advertising, or</li>
                            <li>An application for employment with us.</li>
                          </ul>
                        
                        <p>Moove Media Australia may collect contact details including your name, address, email address and phone numbers.</p>
                        <p>Moove Media Australia may collect personal information as otherwise permitted or required by law.</p>

                      </div>
                    </div>
                  </div>
                  <!--// Accordion Item -->
                  
                  <!-- Accordion Item -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading3">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                          <span>Sensitive Information</span>
                          <i class="fa fa-angle-up" aria-hidden="true"></i>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                      <div class="panel-body">
                        <p>Moove Media Australia will not generally require you to disclose any sensitive information (for example, details of race, religious beliefs, sexual orientation, health information, etc) to us.</p><p>
If you do provide sensitive information to us for any reason, you consent to us collecting that information and to us using and disclosing that information for the purpose which you disclosed it to us and as permitted by the Privacy Act and other relevant laws.</p>

                      </div>
                    </div>
                  </div>
                  <!--// Accordion Item -->

                  <!-- Accordion Item -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading4">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                          <span>How do we collect your personal information?</span>
                          <i class="fa fa-angle-up" aria-hidden="true"></i>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                      <div class="panel-body">
                        <strong>Moove Media Australia collects personal information in a number of ways, including:</strong>
                          <ul>
                            <li>Directly from you when you provide it us (whether in person, on the telephone, via our website or by letter, fax or email)</li>
                            <li>Via our website or when you deal with us online (including through social media)</li>
                            <li>When you enter a competition or promotion</li>
                            <li>From customer surveys</li>
                            <li>From our related companies</li>
                          </ul>

                      </div>
                    </div>
                  </div>
                  <!--// Accordion Item -->

                  <!-- Accordion Item -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading5">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                          <span>Why do we collect your personal information?</span>
                          <i class="fa fa-angle-up" aria-hidden="true"></i>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                      <div class="panel-body">

                        <p>Whenever practical, we endeavour to inform you why we are collecting your personal information, how we intend to use the information and to whom we intend to disclose it at the time we collect your personal information.</p>
                        <strong>We may use or disclose your personal information:</strong>
                        <ul>
                          <li>For the purposes for which we collected it (and related purposes which would be reasonably expected by you)</li>
                          <li>For other purposes to which you have consented, and</li>
                          <li>As otherwise authorised or required by law.</li>
                        </ul>

                        <strong>Some of the specific purposes for which we collect, use and disclose personal information are as follows:</strong>
                        <ul>
                          <li>To facilitate the provision of services you acquire from us and to respond to your queries</li>
                          <li>To contact you (directly or through our service providers) to obtain your feedback and to find out your level of satisfaction with our ​services through surveys</li>
                          <li>To keep you informed of business changes</li>
                          <li>When reasonably necessary to protect or enforce the legal rights or interests of Moove Media Australia (and related entities) or to defend any claims made against Moove Media Australia (and related entities) by any person</li>
                          <li>To verify your identity</li>
                          <li>To facilitate your entry and participation in a competition or promotion</li>
                          <li>To provide goods or services to you or to receive goods or services from you</li>
                          <li>For marketing or client relationship purposes</li>
                          <li>To address any issues or complaints that we or you have regarding our relationship</li>
                          <li>To contact you regarding the above, including via electronic messages such as SMS and email, by mail, by phone or in any other lawful manner.</li>
                        </ul>
                         
                        <p>Some of the personal information we collect will be essential for us to be able to accurately identify who is using our services. Other types of personal information we collect will help us to profile who is using our services and why, to assist us with continuous service improvements.</p>
                        <p>We may send promotional materials, including direct marketing materials, or notifications to you where we believe this information may be of interest to you. We will comply with our obligations under the <em>Spam Act 2003</em> (Cth) and the <em>Do Not Call Register Act 2006</em> (Cth) in relation to such direct marketing communications.</p>
                        <p>You may opt out of receiving direct marketing communications from us at any time.</p>


                      </div>
                    </div>
                  </div>
                  <!--// Accordion Item -->

                  <!-- Accordion Item -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading6">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                          <span>What happens if you don't provide personal information?</span>
                          <i class="fa fa-angle-up" aria-hidden="true"></i>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                      <div class="panel-body">
                        <p>Generally, you have no obligation to provide any personal information requested by us. However, if you choose to withhold requested personal information, we may not be able to provide you with the services that depend on the collection of that information. For example, if we did not collect your personal information when you raised a query regarding lost property on one of our services, we would be unable to notify you if that lost property was later located.</p>

                      </div>
                    </div>
                  </div>
                  <!--// Accordion Item -->

                  <!-- Accordion Item -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading7">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
                          <span>Collection of Information From Our Website</span>
                          <i class="fa fa-angle-up" aria-hidden="true"></i>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                      <div class="panel-body">
                        In order to monitor our website's performance, we may track your IP address, pages accessed, the documents downloaded and your session time. This information is aggregated on an anonymous basis and cannot be used to identify you. We may place a cookie in the browser files of your computer. <strong>This cookie does not contain any personally identifying information but will enable us to :</strong>
                        <ul>
                          <li>Relate your use of the site to information that you have specifically and knowingly provided to us, such as any services we may provide via our website; or</li>
                          <li>Allow your browser to track searches you have performed on our website.</li>
                        </ul>

                      </div>
                    </div>
                  </div>
                  <!--// Accordion Item -->

                  <!-- Accordion Item -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading8">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="false" aria-controls="collapse8">
                          <span>What happens if you don't provide personal information?</span>
                          <i class="fa fa-angle-up" aria-hidden="true"></i>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
                      <div class="panel-body">
                        <p>We may disclose your personal information to third parties, for example to third party contractors and data processing companies. Where we disclose personal information to third parties we will use reasonable commercial efforts to ensure that such third parties only use your personal information as reasonably required for the purpose we disclosed it to them and in a manner consistent with the Australian Privacy Principles.</p>
                        <p>We take all reasonable steps to protect your personal information from misuse and loss, unauthorised access, modification or disclosure. Our personnel are trained to treat personal information so as to maintain confidentiality.  When personal information is no longer required, the information will be destroyed or permanently de-identified.</p>
                        <p>Your personal information will not be 'sold' by Moove Media Australia to any other organisation for that organisation's unrelated independent use.</p>

                      </div>
                    </div>
                  </div>
                  <!--// Accordion Item -->

                  <!-- Accordion Item -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading9">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
                          <span>Does your personal information leave Australia?</span>
                          <i class="fa fa-angle-up" aria-hidden="true"></i>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
                      <div class="panel-body">
                        <p>Your information may be shared between related entities where necessary, including those based overseas (specifically Singapore where Moove Media Australia's ultimate parent company is based).</p>

                      </div>
                    </div>
                  </div>
                  <!--// Accordion Item -->

                  <!-- Accordion Item -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading10">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
                          <span>Information For Job Applicants</span>
                          <i class="fa fa-angle-up" aria-hidden="true"></i>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
                      <div class="panel-body">
                        <p>If you submit a job application to Moove Media Australia, we will use the information provided by you to assess your application. Moove Media Australia may disclose the information contained in your application to contracted service providers for purposes such as screening, aptitude testing and candidate assessments.</p>

                      </div>
                    </div>
                  </div>
                  <!--// Accordion Item -->

                  <!-- Accordion Item -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading11">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
                          <span>How can you access and correct your personal information?</span>
                          <i class="fa fa-angle-up" aria-hidden="true"></i>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                      <div class="panel-body">
                        <p>Moove Media Australia endeavours to maintain your personal information as accurately as reasonable possible.</p><p>
If you would like to access, review, correct or update your personal information, you may contact us at <a href="mailto:info@moovemediaoz.com">info@moovemediaoz.com</a> or <a href="tel:(02) 96901144">(02) 96901144</a>. In your request, please include your email address, name, address and telephone number and we will respond to your request as soon as reasonably practicable.</p><p>
On request, we will provide you with access to the information we hold about you in accordance with the Privacy Act, subject to certain exemptions which may apply. We will provide you with written reasons, including the relevant exemption under the Australian Privacy Principles, if any request is refused.</p>

                      </div>
                    </div>
                  </div>
                  <!--// Accordion Item -->

                  <!-- Accordion Item -->
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading12">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse12" aria-expanded="false" aria-controls="collapse12">
                          <span>How to lodge a complaint</span>
                          <i class="fa fa-angle-up" aria-hidden="true"></i>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
                      <div class="panel-body">
                        <p>If you would like to access your personal information, or have a question or complaint about the collection or use of your information, please write to:</p>
                        <p>
                        <strong>Privacy Officer – Moove Media Australia Pty Ltd</strong><br/>
Suite 104, 15 Belvoir Street<br/>
SURRY HILLS  NSW  2010</p>
<p>We will take any privacy complaint seriously and we request that you co-operate with us and provide any relevant information we might need to assess your complaint. We will provide a response as soon as practicable, but within 30 days. If you are not satisfied with our response to your complaint, or at any time, you may refer your complaint to the Office of the Australian Information Commissioner (<a href="www.oaic.gov.au">www.oaic.gov.au</a>).</p>
<p>All requests and complaints will be dealt with in accordance with this Privacy Policy and the Australian Privacy Principles.</p>

                      </div>
                    </div>
                  </div>
                  <!--// Accordion Item -->



                </div>


                <!--// Content Policies -->

            </div>
          </div>
        </section>

    </div><!--// End Body content -->
    
    <!-- Footer content -->
    <!--#include file="Includes/footer.htm" -->
    <!--// Footer content -->
    <!--#include file="Includes/_scripts.htm" -->
</body>
</html>